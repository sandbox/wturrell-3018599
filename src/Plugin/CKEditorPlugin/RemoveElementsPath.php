<?php

/**
 * @file
 * Definition of \Drupal\ckeditor_remove_elementspath\Plugin\CKEditorPlugin\RemoveElementsPath
 */

namespace Drupal\ckeditor_remove_elementspath\Plugin\CKEditorPlugin;

use Drupal\ckeditor\Plugin\CKEditorPlugin\Internal;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "Remove Elements Path" plugin.
 *
 * @CKEditorPlugin(
 *   id = "remove_elements_path",
 *   label = @Translation("Remove elements path from CKEditor status bar")
 * )
 */
class RemoveElementsPath extends Internal {

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    // Get default config.
    $config = parent::getConfig($editor);

    $config['removePlugins'] = 'elementspath';

    return $config;
  }

}